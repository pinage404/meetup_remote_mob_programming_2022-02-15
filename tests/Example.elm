module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)


type Letter
    = Correct
    | Absent
    | Present


suite : Test
suite =
    describe "wordle"
        [ test "no letters match" <|
            \_ -> wordle "chien" "aaaaa" |> Expect.equal [ Absent, Absent, Absent, Absent, Absent ]
        , test "first letter match" <|
            \_ -> wordle "chien" "caaaa" |> Expect.equal [ Correct, Absent, Absent, Absent, Absent ]
        , test "first letter match 2" <|
            \_ -> wordle "chien" "cbbbb" |> Expect.equal [ Correct, Absent, Absent, Absent, Absent ]
        , test "first letter match 3" <|
            \_ -> wordle "bbbbb" "baaaa" |> Expect.equal [ Correct, Absent, Absent, Absent, Absent ]
        , test "second letter match" <|
            \_ -> wordle "chien" "bhbbb" |> Expect.equal [ Absent, Correct, Absent, Absent, Absent ]
        , test "first letter at the wrong position" <|
            \_ -> wordle "chien" "hbbbb" |> Expect.equal [ Present, Absent, Absent, Absent, Absent ]
        ]


wordle : String -> String -> List Letter
wordle find guess =
    let
        findLetters =
            String.split "" find

        firstLetterFind =
            List.drop 0 findLetters |> List.head |> Maybe.withDefault ""

        firstLetterGuess =
            List.drop 0 letters |> List.head |> Maybe.withDefault ""

        letters =
            String.split "" guess
    in
    if firstLetterGuess == firstLetterFind then
        Correct :: wordleSecond 1 find guess

    else if firstLetterGuess == "h" then
        Present :: wordleSecond 1 find guess

    else
        Absent :: wordleSecond 1 find guess


wordleSecond : Int -> String -> String -> List Letter
wordleSecond position find guess =
    let
        findLetters =
            String.split "" find

        secondLetterFind =
            List.drop position findLetters |> List.head |> Maybe.withDefault ""

        secondLetterGuess =
            List.drop position letters |> List.head |> Maybe.withDefault ""

        letters =
            String.split "" guess
    in
    if secondLetterGuess == secondLetterFind then
        [ Correct, Absent, Absent, Absent ]

    else
        [ Absent, Absent, Absent, Absent ]
